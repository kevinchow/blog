---
layout: post
title: !binary |-
  Q29taW5nIG9mIEFnZSBEYXkg5oiQ5Lq644Gu5pelLCBTZWlqaW4gbm8gSGk=
created: 1294686960
---
<p>
	<font color="#000000" face="sans-serif"><span style="line-height: 19px; ">Yesterday January 11 was a public holiday in Japan-Coming of age day is halt on second Monday in January. It was to celebrate young people becoming adult when they reach 20 years old.&nbsp;</span></font></p>
<p>
	<span style="color: rgb(0, 0, 0); font-family: sans-serif; line-height: 19px; ">Coming of age ceremonies&nbsp;<span style="font-weight: normal; ">(<span lang="ja">成人式</span>&nbsp;<i>Seijin-shiki</i>)</span>&nbsp;are generally held in the morning at local city offices, most girl will wear Kimono and guys will wear western style suit and some time guys will wear&nbsp;men&#39;s&nbsp;traditional Kimono callHakama.&nbsp;</span>During the ceremonies, official will speech in the city hall and teach those young people become a good citizen. Which mean pay taxes and vote for them.</p>
<p>
	&nbsp;</p>
<p>
	<a href="https://sites.google.com/site/cyuhin/Japan_survival_guide_blog/comingofagedayseijinnohiseijinnohi/Coming-of-age-day-5.jpg?attredirects=0" imageanchor="1" style="color: rgb(0, 128, 187); background-image: none; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: initial; border-bottom-style: none; border-bottom-width: initial; border-bottom-color: initial; "><img src="https://sites.google.com/site/cyuhin/_/rsrc/1295149735511/Japan_survival_guide_blog/comingofagedayseijinnohiseijinnohi/Coming-of-age-day-5.jpg?height=240&amp;width=320" style="cursor: default; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; width: 267px; height: 200px; " /></a></p>
<div style="background-color: transparent; ">
	<div style="background-color: transparent; display: block; text-align: left; ">
		<div style="background-color: transparent; margin-top: 5px; margin-bottom: 0px; display: block; margin-right: auto; text-align: left; ">
			<p>
				&nbsp;</p>
			<div style="background-color: transparent; ">
				<div style="background-color: transparent; display: block; text-align: left; ">
					<div style="background-color: transparent; margin-top: 5px; margin-bottom: 0px; display: block; margin-right: auto; text-align: left; ">
						&nbsp;</div>
				</div>
			</div>
			<div style="background-color: transparent; ">
				<div style="background-color: transparent; display: block; text-align: left; ">
					<div style="background-color: transparent; margin-top: 5px; margin-bottom: 0px; display: block; margin-right: auto; text-align: left; ">
						If the weather is cold, they will add a muffler on top of the Kimono.&nbsp;</div>
				</div>
			</div>
		</div>
		<div style="background-color: transparent; display: block; text-align: left; ">
			<div style="background-color: transparent; display: block; text-align: left; ">
				<font color="#000000" face="sans-serif"><span style="line-height: 19px; "><a href="https://sites.google.com/site/cyuhin/Japan_survival_guide_blog/comingofagedayseijinnohiseijinnohi/Coming-of-age-day-4.jpg?attredirects=0" imageanchor="1" style="color: rgb(0, 128, 187); background-image: none; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: initial; border-bottom-style: none; border-bottom-width: initial; border-bottom-color: initial; "><img border="0" height="400" src="https://sites.google.com/site/cyuhin/_/rsrc/1295149735466/Japan_survival_guide_blog/comingofagedayseijinnohiseijinnohi/Coming-of-age-day-4.jpg?height=400&amp;width=300" style="cursor: default; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-style: initial; border-color: initial; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; " width="300" /></a></span></font></div>
		</div>
		<div style="background-color: transparent; display: block; text-align: left; ">
			<font color="#000000" face="sans-serif"><span style="line-height: 19px; ">Most people cannot wear Kimono by themselves, so they usually need help from their mother or go to a special&nbsp;salon.Kimono&nbsp;is very expensive, so people may borrow from their relative or rent at a shop.</span></font></div>
		<div style="background-color: transparent; display: block; text-align: left; ">
			&nbsp;</div>
		<font color="#000000" face="sans-serif"><span style="line-height: 19px; "><img border="0" height="320" src="https://sites.google.com/site/cyuhin/_/rsrc/1295149735320/Japan_survival_guide_blog/comingofagedayseijinnohiseijinnohi/Coming-of-age-day-1.jpg?height=320&amp;width=240" style="cursor: default; " width="240" /></span></font></div>
</div>
<div style="background-color: transparent; ">
	<font color="#000000" face="sans-serif"><span style="line-height: 19px; ">For the young people, this is one of the most important ceremonies in their life. They got a chance to meet their friends from junior high school or even&nbsp;kindergarden&nbsp;if they still live in their home town. After the ceremonies, young people will hang out with their friend in a group and go to karaoke, drink or party.</span></font></div>
<div style="background-color: transparent; ">
	<font color="#000000" face="sans-serif"><span style="line-height: 19px; "><img border="0" height="240" src="https://sites.google.com/site/cyuhin/_/rsrc/1295149735419/Japan_survival_guide_blog/comingofagedayseijinnohiseijinnohi/Coming-of-age-day-3.jpg?height=240&amp;width=320" style="cursor: default; " width="320" /></span></font></div>
