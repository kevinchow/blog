---
layout: post
title: Update this blog with SSL and alias domain with www redirect
created: 1413262105
---
First, I registered a new domain for this website - chow.red, short and simple, right? I know you will love it :D

Second, for your security concern, I enabled Full SSL (strict) on CloudFlare alone with free "Class 1 SSL Certificate" on the server using StartSSL.
This is not an advertisement or anything like that, I just want to share freebie like this with you all.

* FREE SSL (startssl.com)
* FREE CDN (CloudFlare.com)

Who don't like freebies? I know I do :)

Here is how I did it.

1. Sign up on  https://www.startssl.com/ and CloudFlare (Free SSL enabled on Oct 9)
2. Create your key and certificate request (your_domain.csr) with the following code

        openssl req -new -keyout your_domain.key -out your_domain.csr
3. Verify your domain with the "Validations Wizard" in startssl.com
4. Use "Certificates Wizard" to create your certificate, copy your_domain.csr and click next
5. copy the cert to your server your_domain.crt
6.  Setup your nginx config

        //  Redirect all your www and non-www traffic to https non-www domain
        server {
            listen 80;
            server_name  your_domain.com www.your_domain.com;
            rewrite ^(.*) https://your_domain.com$1 permanent;
        }
        server {
            listen 443 ssl;
            ssl on;
            ssl_certificate      /etc/ssl/certs/your_domainSSL.crt;
            ssl_certificate_key  /etc/ssl/certs/your_domain.key;
            ssl_client_certificate /etc/pki/tls/certs/ca-bundle.crt;
            server_name  www.your_domain.com;
            rewrite ^(.*) https://your_domain.com$1 permanent;
        }

        // Set up the SSL
        server {
            listen 443 ssl;
            server_name your_domain.com;
            root   /var/www/your_domain.com/root;
            ssl on;
            ssl_certificate      /etc/ssl/certs/your_domainSSL.crt;
            ssl_certificate_key  /etc/ssl/certs/your_domain.key;
            ssl_client_certificate /etc/pki/tls/certs/ca-bundle.crt;
            location @rewrite {
                  # Some modules enforce no slash (/) at the end of the URL
                  # Else this rewrite block wouldn't be needed (GlobalRedirect)
                  rewrite ^ /index.php last;
            }
            location @rewrite {
                rewrite ^/(.*)$ /index.php?q=$1;
            }
            location ~ ^/sites/.*/files/styles/ {
                try_files $uri @rewrite;
            }
        }
7. Set you your CDN on CloudFlare, and enable FULL SSL (strict).
8. Enjoy looking at the green lock next to your URL.
