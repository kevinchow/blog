---
layout: post
title: Skateboarding in Japan pt.1
created: 1297148675
---
<div>
	&nbsp;</div>
<div>
	Today I want to&nbsp;share some of my experiences skateboarding here in Japan. I have been skateboarding for about 11 years. I was never all that good (and still am not) but I was swallowed by the culture as a pre-teen and have been involved somehow ever since. As a kid, I skated all day everyday. Summer was just skateboarding. Every other season was just a battle against snow to skate more. In high school I only skated rarely, and took up other sports. In college I skated to and from classes but rarely just went out to skate for fun. When I studied abroad, I brought my board over and skated around the streets of Ueno plenty of times with my friend Rajko. On our group trips to Hiroshima and Kyoto, while everyone else was looking at temples and shrines, I was just skating and playing pachinko. After college I was skating more and more because for the first time since I was a child, I had tons of free time again. Then I moved to Japan.</div>
<div>
	&nbsp;</div>
<div>
	I didn`t bring my board with me this time in the interest of packing as light as I could, but after receiving my first pay-check, I couldn`t help myself and bought a board. I mostly just took the Yamanote up to Ueno and skated there when I had some time, but after I moved to Funabashi, that was when I&nbsp;started falling in love again.</div>
<div>
	The first place I found to skate was Shinsui Park.</div>
<div>
	<img alt="" src="/images/44665_107506262641429_100001461669344_74004_7263362_n.jpg" style="width: 720px; height: 479px" /></div>
<div>
	&nbsp;</div>
<div>
	Shinsui Park is right next to Lala Port in Funabashi and it`s a pretty darn good skate spot. I came across it exploring one day and saw two middle school kids skating there. I joined them, while they were waiting for their friends to return from the mall. We skated and skated and it was great and finally the friends showed up. I didn`t expect these guys to be 20 and 28 years old, but they were really cool. We spent the rest of the day skating at the park, and then under the local highway in an abandoned parking lot after it started raining. After the middle schoolers went home, I asked the other two if they`d want to go out for a drink that night. They agreed, we drank quite a bit, and I`ve been hanging out with the group ever since.</div>
<div>
	&nbsp;</div>
<div>
	After I joined the group, we began meeting more and more skaters and began to have a network of friends throughout Chiba. We met friends at Shinsui Park and&nbsp;at the local skateparks as well.<img alt="" src="/images/40664_106649579393506_100001453943752_58100_3607878_n.jpg" style="width: 720px; height: 540px" /></div>
<div>
	^These guys are fun^</div>
<div>
	&nbsp;</div>
<div>
	So when I started skating with them everyday, it was great. I loved having a group of good friends to hang out with everyday and thats what made me keep hanging out. But after a couple months I began improving very fast and I started to love skateboarding again for the first time in years. I would go out by myself just to practice, which I hadn`t done since I was a kid. But I was hooked. So I kept on skating and making more friends.</div>
<div>
	I think that one of the things about skating that I love is that it draws similiar people together. I suppose all hobbies do that, but this one in particular tends to bring in a very specific kind of person. I became friends with the owner of the local skateshop, Murasaki Sports, and he`s kept me updated with local skate events. Language has never been an issue while skating and I`ve made tons of friends through it. One problem for a lot of men that come to Japan is making friends with other men. It`s no problem at all to befriend women, but for some reason or another it`s more difficult with men. For the first half year I was here, I was with mostly Japanese women and as a result of that, I didn`t know how to interact with Japanese men very well unless they spoke a lot of english.&nbsp;Through skateboarding, I learned how to communicate better and probably wouldn`t have had the chance to use my Japanese as much as I do, so it would still be terrible.</div>
<div>
	&nbsp;</div>
<div>
	If you`re a skater, interested in starting, or just like to watch, here are some places to check out:</div>
<div>
	--Shinsui Park-Funabashi-Chiba</div>
<div>
	Shinsui is full of skaters day and night during the summer and there are many people there during the spring winter and fall as well. Not a real skate park, but has some good ledges, benches and is lit up at night. (Also, across the bay, is an empty parking lot where people have 2 hidden launch ramps and a box. Skater will often come there at night and bring more equipment to skate)</div>
<div>
	--Maihama Skate Park-Maihama-Chiba</div>
<div>
	Take the train to Tokyo Disney, take a left out the main gates, go down the stairs and head straight. At the big street, take a left and skte for about 5 minutes then cross the street. Go into the park and to the left of the playground is Maihama skate park. It isn`t too big, but there`s a&nbsp;mini-ramp, 2 rails, box, and a concrete mini-ramp as well. There are concrete banks and big steps as well. There is also a small open area at the entrance where there are usually freestyle skaters hanging out. This place is popular for all skill levels so there are often a few kids here too.</div>
<div>
	--Daikien-Matsudo-Chiba</div>
<div>
	Daikien is&nbsp;a 24 hour mini-amusement park. There is a huge helipad that skaters utilize as well as a mini ramp and a couple of banks. The rails here are pretty sketchy but you can&nbsp;tell that just by looking at them.&nbsp;There isn`t tons to work with here, but the 24 lights are nice, as well as having food and drinks available as well. The people here are usually pretty friendly as well. I met a couple pros from the Lakai Japan team here, and it`s a pretty well-known spot.</div>
<div>
	--MAPS Chiba Skatepark-Chiba City, Chiba</div>
<div>
	This one is interesting as it`s located on top of a department store. You can`t ever really see over the edge very close so those with a fear of heights have nothing to worry about. The park here is a bit old, but tons of fun to skate. Last time I was there (which was a while now, like 5 months) they were building a new mini-ramp, and it was looking good. This park is nice to skate because there is always music blasting and the regular crew that skates here are very friendly.</div>
<div>
	--Makuhari Seaside Park-Makuhari Messe-Chiba</div>
<div>
	This is a park that a friend and I found a few weeks ago. It`s really good for intermediate level skaters and there aren`t too many people around so it`s quite peaceful.</div>
<div>
	&nbsp;</div>
<div>
	Other than that, there are tons of good places everywhere. These are just the places that I have skated most since I came to Japan. There are tons of good spots in Shinjuku and Ueno as well, so just look out for some skaters and ask them where there going!</div>
<div>
	&nbsp;</div>
