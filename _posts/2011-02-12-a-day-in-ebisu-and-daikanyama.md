---
layout: post
title: !binary |-
  QSBkYXkgaW4gRWJpc3UgYW5kIERhaWthbnlhbWEg5oG15q+U5a+/44Go5Luj
  5a6Y5bGx
created: 1297485811
---
<p>
	Last week was the busiest week I had so far, mostly because of the part time job. It really hard for me to take some time off and update the blog, but I finally get Friday off because of the&nbsp;National Foundation Day&nbsp;- 建国記念の日&nbsp;in Japan. A week ago I already plan ahead for this weekend, I was so exciting and looking forward to it. Yesterday was the day I waited for a week, and it happened to be a snowing day. Lucky it wasn&#39;t too much, and it melt immediately. The snow just keep coming all day long, but that is not going to stop me.&nbsp;</p>
<p>
	<img alt="" src="/images/Oji.jpg" style="width: 500px; height: 335px; " /></p>
<p>
	&nbsp;</p>
<meta charset="utf-8" />
<p>
	On my way to Ebisu I find a interesting place in Ouji Station 王子駅, it seem to be an&nbsp;abandoned places that used to be a restaurant street. I come to this station everyday but I never see any light come from those buildings and it look really strange. &nbsp;</p>
<p>
	<img alt="" src="/images/ebisu_atre.jpg" style="width: 500px; height: 333px; " /></p>
<p>
	&nbsp;</p>
<meta charset="utf-8" />
<p>
	After I arrvied in <a href="http://maps.google.co.jp/maps/place?ftid=0x60188b4046e3f71d:0xb1bb4db58ccdae2d&amp;q=ebisu&amp;hl=ja&amp;dtab=0&amp;sll=35.64669,139.710106&amp;sspn=0.016744,0.020496&amp;brcurrent=3,0x60188cb2eb3108d1:0xf11cd9b2395b6677,0&amp;ie=UTF8&amp;ll=35.656111,139.694724&amp;spn=0,0&amp;t=h&amp;z=15">Ebisu</a>, I come acrossed a shopping mall call &quot;<a href="http://www.atre.co.jp/ebisu/">Atre</a>&quot; but it was very small compare with &quot;Lalaport&quot; and most of the shops sell either fashion clouthing or food especially chocolate because of Valentine&#39;s Day. It was fun to walk around for an hour or two but there was nothing to buy.&nbsp;<img alt="" src="/images/ebisu_atre_garden.JPG" style="width: 500px; height: 375px; " /></p>
<p>
	&nbsp;</p>
<meta charset="utf-8" />
<p>
	The only special about this shopping mall is the &quot;garden&quot; on the top floor of the building, which is a nice place to take a rest and enjoy the city view of Ebisu. If you want to visit this place, make sure to check the weather report because the garden will close if it is rainning. After about an hours I decide to leave this place and look for something more interestisng. I follow the main street to the&nbsp;Daikanyama&nbsp;代官山, a very interesting place to shopping. The shops in&nbsp;Daikanyama sell all kind of clouthings and accessories, each store are specialized in different area. Like hat, shose, American style clothing that made in Japan and many others. While I walking around I find one particular store interested me the most, &quot;<a href="http://www.hrm.co.jp/okura/">Okura</a>&quot;- a store that sell Japanese style&nbsp;natural clothing also traditional. <a href="http://maps.google.co.jp/maps/place?hl=ja&amp;ie=UTF8&amp;q=%E4%BB%A3%E5%AE%98%E5%B1%B1+okura&amp;fb=1&amp;gl=jp&amp;hq=okura&amp;hnear=%E4%BB%A3%E5%AE%98%E5%B1%B1%E9%A7%85%EF%BC%88%E6%9D%B1%E4%BA%AC%EF%BC%89&amp;cid=16800579529809435937&amp;t=h&amp;z=14">Map to Okura</a></p>
<p>
	<img alt="" src="/images/Okura.png" style="width: 500px; height: 200px; " /></p>
<p>
	The store is very unique and one of a kind, it decorate with traditional sliding door, interior, and items that&nbsp;represent japan.</p>
<meta content="text/html;charset=UTF-8" http-equiv="Content-Type" />
<p>
	<img alt="" src="/images/Okura_item.png" style="width: 500px; height: 146px; " /><img alt="" src="/images/okura_cloth.png" style="width: 500px; height: 151px; " /></p>
<p>
	Even if you are not intented to buy anything, this is the place visit and take a look. The stores across the street are very intersting too, the one on the left sell American style clothing and the right one sell British style clothing.</p>
<p>
	<img alt="" src="/images/Across_okura.png" style="width: 500px; height: 227px; " /></p>
<meta content="text/html;charset=UTF-8" http-equiv="Content-Type" />
<meta content="text/html;charset=UTF-8" http-equiv="Content-Type" />
<p>
	<img alt="" src="/images/Daikanyama.jpg" style="width: 500px; height: 335px; " /></p>
<p>
	On the way back home come across a store that only sell &quot;pink&quot; stuff.</p>
<p>
	<img alt="" src="/images/kasuzi_noodle_shop.jpg" style="width: 500px; height: 335px; " /><img alt="" src="/images/kazuki_noodle.jpg" style="width: 500px; height: 335px; " /></p>
<p>
	At the end of the day, came back to Ebisu and ate at Kazuki, for a low price of &yen;800 you get this big bow of noodle and it tastes really good. After I come back and google search about this store, it turns out that this store is very famous and they have branch in&nbsp;Haneda Airport too.</p>
<meta charset="utf-8" />
<p>
	&nbsp;</p>
<p>
	&nbsp;</p>
<p>
	&nbsp;</p>
<meta charset="utf-8" />
<p>
	&nbsp;</p>
