---
layout: post
title: Japan Visa
created: 1315540267
---
<p>
	<span style="font-size:12px;">Getting a work visa in Japan is one of the hardest things to do by yourself, most company will hire a layer or an immigration specialist to handle the visa application to ensure their employee get that visa. As of this article written - September 2011, hiring an immigration specialist will cost you around &yen; 120,000 about 1500 USD. If you are reading this that mean you want to save a buck. Then you come to the right place, if you have any question please feel free to leave a comment below and I will try my best to answer your questions.</span></p>
<div>
	<span style="font-size:12px;">A work visa generally require you to have a job first before you can apply. If you already have a job you can skip this paragraph and continue reading this article. Most Japanese company will not accept phone interview nor online application, so you need to come to Japan with a tourist visa, then you need to hand written your resume and mail it to the office and arrange an face-to-face interview, unless you are applying a new generation company or working in IT industry. Otherwise get your pen out and start practicing your handwriting now. After your resume reviewed by HR and accepted to an interview, don&#39;t expect you are done. Japanese company generally have 2 to 3 interviews before they will give you an offer. First you will interview by HR department manager then your future manager and last the president of the company. After you given an offer, you will need to apply a&nbsp;<a href="http://www.mofa.go.jp/j_info/visit/visa/long/visa1.html"><span style="color:#008000;">Certificate of Eligibility&nbsp;</span></a>from a Japanese embassy in your own country. That mean you need to go leave Japan first, unless you a on a student visa. An immigration specialist told me that generally take at lease 2 months.</span></div>
<div>
	&nbsp;</div>
<div>
	<span style="font-size:12px;">If you given an offer with a student visa, you are allow to apply for <a href="http://www.immi-moj.go.jp/english/tetuduki/kanri/shyorui/02.html"><span style="color:#008000;">Change of Status of Residence</span></a>&nbsp;which will allow you to &quot;change&quot; to a work visa without leaving Japan.&nbsp;An immigration specialist told me that generally take at lease 1 month. Note: you student visa end when you graduate from your university not according to the day on the visa, whichever come first. After you graduated from your university you have 3 months to change it to a special &quot;job hunting visa&quot;. This&nbsp;</span>special visa allow you to stay in Japan to look for a job within a year period.</div>
<div>
	&nbsp;</div>
<div>
	<span style="font-size:12px;">Requirements for a working visa: Engineer. For other occupation please click here. (<a href="http://www.immi-moj.go.jp/english/tetuduki/kanri/shyorui/Table3-1.html">Detail</a>)</span></div>
<div>
	<ol>
		<li>
			<span style="font-size:12px;"><a href="http://www.immi-moj.go.jp/english/tetuduki/kanri/shyorui/02-format.html">Application</a></span></li>
		<li>
			<span style="font-size:12px;">Copies of the company registration and a statement of profit and loss of the recipient organization.</span></li>
		<li>
			<span style="font-size:12px;">Materials showing the business substance of the recipient organization.</span></li>
		<li>
			<span style="font-size:12px;">A diploma or a certificate of graduation with a major in the subject regarding the activity of the person concerned, and documents certifying his or her professional career.</span></li>
		<li>
			<span style="font-size:12px;">Documents certifying the activity, its duration, position and the remuneration of the person concerned.</span></li>
	</ol>
	<p>
		After you get everything together go to your regional immigration office in charge of your intended address (Refer to your regional immigration office or immigration information center.)</p>
	<p>
		After you get your Visa don&#39;t forget to apply for a re-entry permit right away it only take you 30 minute otherwise you will void your visa when you travel out of Japan. Other Document you may need are <a href="http://www.immi-moj.go.jp/english/tetuduki/kanri/hituyou_syorui.html">here</a>.</p>
	<p>
		I may miss a step or two, if you have something unclear feel free to let me know and I will add those information.</p>
	<p>
		&nbsp;</p>
	<h3 class="r" style="font-size: medium; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; display: inline; ">
		<a class="l noline" href="http://www.mofa.go.jp/mofaj/" style="font-family: arial, sans-serif; color: rgb(34, 0, 193); cursor: pointer; outline-width: 0px; outline-style: initial; outline-color: initial; ">外務省 -&nbsp;<em style="font-weight: bold; font-style: normal; ">Ministry of Foreign Affairs</em>&nbsp;of Japan (<em style="font-weight: bold; font-style: normal; ">MOFA</em>)</a></h3>
</div>
