---
layout: post
title: Snowboarding pt.1
created: 1298251013
---
<p>
	よー！</p>
<p>
	Today I want to share a bit about snowboarding in Japan. I don`t have tons of experience with snowboarding (this year&nbsp;is my first real season) but I`ll tell you what I know!</p>
<p>
	Snowboarding is great. Simply put, it`s&nbsp;a rush and when you get the hang of it, it`s hard to find a better feeling. I went on my first trip 3 years ago when I was an exchange student. I went with 4 Japanese girls who were all experienced boarders, but didn`t speak that much english, 1 American who had gone one time before but by no means was accustomed to it, and 1 Malaysian who had never touched a board in his life. We went to some mountain in Hakuba, Nagano, and it was absolutely beautiful. At the base of the mountain there was no snow and I didn`t even need my jacket despite it being December. But after the first lift up the mountain, it was like Minnesota again. Snow was everywhere, and it was deep.</p>
<p>
	So when it came to actually riding, the only advice that our Japanese friends could give us was to fall like a leaf from a tree down the hill. We had no idea at all and the first hour or two was us on the falling over and getting up again and then making it a foot further and repeating. After lunch I started watching the girls and later that day I got a very rough grasp on how to ride. That night, as I was falling asleep all that I could think about was leaning forward to turn right and being able to carve. The next day I could do it, and I just practiced carving and riding all day. We went home and I didn`t snowboard for 3 years until...</p>
<p>
	Fall 2010. I had been skateboarding all summer with my friends but it turns out that their main interest was snowboarding all along. One weekend, Yuuki (one of the friends)&nbsp;was going to an indoor course in Ibaraki prefecture. He asked if I wanted to come along and I said yes. I figured that I had forgotten everything I learned on those 2 days 3 years before, and I was right. I wanted to see what it was like though and it`s always fun hanging out with my friends. So we went and the place was tiny. There wasn`t enough room to carve at all, just a huge jump, a smaller jump, and a rail. I was a bit disappointed but decided to just try my best anyway. I ended up barely being able to go down the short hill, and ripped up my forearm pretty badly on the snow/ice. We took a break and one corndog and a small bottle of whiskey later I was up and at it again. I was a little bit better this time, but was still awful. I wasn`t all that down on myself however, because I knew that if I had a big hill, that I`d be able to pick it all up again quickly.</p>
<p>
	That takes me to 2011 winter. I went snowboarding and picked everything up again within the first hour on the hill and spent the first day of our New Years trip practicing. Day 2 I tried jumps out and got more aggresive, which went really well. The third day, after lunch, the physical and mental fatigue set in and I couldn`t do anything except ride down the hill, which was pretty difficult at that time. I went again a couple weeks ago and had a great time and even managed to land my first 360.</p>
<p>
	&nbsp;</p>
<p>
	So that`s my experience with the scene, but what comes along with it all is the fun part.</p>
<p>
	Going on a snowboarding trip is a great way to bond with friends, and if you have a chance to go with Japanese men, it`s a good way to make some closer guy friends as well. Driving to the mountain is fun and being on the road in a tiny car/van, is always a good way to get closer to people. The days are snowboarding with the occasional beer at lunch, but for those that can stay awake at night, it`s just drinking. Ususally I have been situated in a resort town of some sort, and hitting the streets or just staying in the hotel and getting messy is tons of fun.</p>
<p>
	Another thing is that snowboarders here take their hobby quite seriously usually. My friends gave me their old gear, including a used board (all in great condition). I still need to rent boots because my feet are too big for their extras, but snowboarders here tend to have plenty of excess gear. In the more crowded areas of downtown, it`s rare to have space for all of that extra gear, but where I`m living, there`s more than enough space in most apartments for the gear.</p>
<p>
	Places to go (places that I`ve been):</p>
<p>
	Hakuba, Nagano&nbsp;was the first place I went to and it was great. There are tons of slopes around the area and it`s pretty close to Tokyo.</p>
<p>
	Ikebane, Niigata is where I`ve spent the rest of my time boarding. We were near Myoko-yama, and the area is absolutely beautiful. We`ve gone to Ikebane-taira which has tons of courses and the two &quot;park&quot; areas are pretty nice. The Akamura Onsen area is also pretty nice. If you find your way to the top of the mountain, the off trail areas are very challenging and a lot of fun. The park here is a bit limited, but the courses through some of the woods are long and interesting.</p>
<p>
	&nbsp;</p>
<p>
	When all is said and done, if my friends didn`t snowboard, I probably woudn`t. I really enjoy it and it`s a lot of fun, but really it`s a way for me to bond with friends and make new ones too.</p>
