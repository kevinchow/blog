---
layout: post
title: 'Newrelic''s nginx plugin install '
created: 1371700279
---
<p>Today,&nbsp;Newrelic announced a new open platform for you to install your own plugin to monitor your own stack.</p>

<p>It featured a list of default plugin&nbsp;- http://newrelic.com/platform&nbsp;that you can use. Since</p>

<p>I use Nginx on my server, so I decided to give it a try.&nbsp;</p>

<p>First the plugin require you to install Ruby and&nbsp;<span style="color: rgb(68, 68, 68); font-family: HelveticaNeueWeb, 'Helvetica Neue', Helvetica, Arial, Verdana, sans-serif; font-size: 14px; line-height: 17.984375px;">bundler for Ruby, after that you need to download the tar file from&nbsp;</span>http://nginx.com/nrform.html.</p>

<p>Download the file to your server and decompress it, then run</p>

<blockquote>
<p>bundle install</p>
</blockquote>

<p>If you have problem with the gem&#39;s json and see an error like this</p>

<blockquote>
<p>mkmf.rb can&#39;t find header files for ruby at /usr/share/ruby/include/ruby.h​</p>
</blockquote>

<p>Make sure you install ruby-devel and other require dependencies&nbsp;</p>

<blockquote>
<p>sudo yum install ruby-rdoc ruby-devel</p>

<p>yum install make</p>

<p>yum install gcc</p>
</blockquote>

<p>Also the Gemfile&#39;s&nbsp;git repository path is incorrect, change it to use HTTPS. You may get a message similar to the follwoing:</p>

<blockquote>
<p>git clone git@github.com:/newrelic-platform/newrelic_plugin.git<br />
Cloning into &#39;newrelic_plugin&#39;...<br />
Permission denied (publickey).<br />
fatal: Could not read from remote repository.</p>
</blockquote>

<p>Change it to this.</p>

<blockquote>
<p>gem &quot;newrelic_plugin&quot;, :git =&gt; &quot;https://github.com/newrelic-platform/newrelic_plugin.git&quot;, :branch =&gt; &#39;release&#39;</p>
</blockquote>

<p>Now &quot;build install&quot; again and you should be fine. Remember to follow the README file and change the account id. I skip those steps, and just descripted the PROBLEM you may see during the installation here.</p>
