---
layout: post
title: Chef Server Installation Guide for Amazon linux
created: 1397202463
---
<ul>
 <li>Chef Server Installation Guide for Amazon linux</li> 
First find the right version to install from http://www.getchef.com/chef/install/ as of this writing, I am usign 11.0.12.
<code>rpm -Uvh https://opscode-omnibus-packages.s3.amazonaws.com/el/6/x86_64/chef-server-11.0.12-1.el6.x86_64.rpm</code>
// Run this to configure the sever, if you have problem checkout the trouble shooting guide below.
<code>chef-server-ctl reconfigure</code>
// Check Chef server config
 <code>chef-server-ctl test</code>
<li>Set /etc/hosts otherwise you may not be able to login on the web UI</li>
<code>xx.xxx.xxx.xxx your.awesome.domain.com
127.0.0.1 localhost localhost.localdomain</host>

If you are installing Chef server on Amazon Linux micro instace you may run across the following problem:
 <li>Stuck on ruby_block[supervise_rabbitmq_sleep] action run</li> 

 Add the following line to /opt/chef-server/embedded/cookbooks/runit/recipes/default.rb will solve the problem.
<code> when "amazon"
  include_recipe "runit::upstart"
</code> 


<li>Memory</li> 
Make sure you have enough memory and swap space

// Run this command to create swap space
<code>dd if=/dev/zero of=/swap_1 bs=2048 count=1000000
mkswap /swap_1
swapon /swap_1
</code>

// Add the follow line to /etc/fstab so it will auto mount after reboot
<code>/swap_1 swap swap defaults 0 0</code>

// Now you should be able to see the swap space you just created with this command
<code>free -tom</code>

<li>Dependency</li>
You may have problem configure your Chef client(workstation) on your Mac,  when you run "knife configure --initial" it will show you the following error:
<code>Ohai::Exceptions::DependencyNotFound: Can not find a plugin for dependency os</code>

That is the problem of chef client 11.12, try to install 10.32 first and install 11.12 again. This will bypass the problem in the mean time, when the patch will apply to the future release.(https://tickets.opscode.com/browse/CHEF-5211)

<code>curl -L https://www.opscode.com/chef/install.sh | sudo bash -s -- -v 10.32.2</code> 
</ul>
