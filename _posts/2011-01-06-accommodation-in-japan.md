---
layout: post
title: Accommodation in Japan
created: 1294353300
---
<p>
	After you arrived in Japan, the very first thing you may want is an accommodation. Staying in a hotel is one of the option for a short term visit, but if you are staying in Japan for a long term this is not the best option. Because in Japan all hotels are charge according to the number of people staying and not by the number of room you use.&nbsp;Staying in a Cheap hotel near Arakawa (荒川) will cost around &yen;2000 a night for every person. If you are staying in a decent 3 to 4 star hotel, the cost will be higher around &yen;3000~&yen;6000 per person depend on the location.</p>
<div style="background-color: transparent; ">
	<br />
	<div style="background-color: transparent; ">
		<b>Here is some of your option to stay in Japan for Cheap: (for short term visit)</b></div>
	<div style="background-color: transparent; ">
		&nbsp;</div>
	<div style="background-color: transparent; ">
		<img alt="" src="/images/Couch_surfing.jpeg" style="width: 267px; height: 200px; " /></div>
	<div style="background-color: transparent; ">
		&nbsp;</div>
	<div style="background-color: transparent; ">
		&nbsp;</div>
	<div style="background-color: transparent; ">
		<div style="background-color: transparent; ">
			<a href="http://www.couchsurfing.org/" rel="nofollow" style="color: rgb(0, 128, 187); " target="_blank">Couch surfing</a></div>
		<div style="background-color: transparent; ">
			<ul>
				<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
					It is similar to homestay, usually is free but you should prepare some gift for your host.</li>
				<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
					No services, you are expect to clean up after yourself.</li>
				<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
					Only for few day, usually no more than 3 days.</li>
			</ul>
			<p style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
				<img alt="" src="/images/Homestay.jpg" style="width: 258px; height: 200px; " /></p>
		</div>
		<div style="background-color: transparent; ">
			<div style="background-color: transparent; ">
				<a href="http://www.homestayweb.com/" rel="nofollow" style="color: rgb(0, 128, 187); " target="_blank">Homestay</a></div>
			<div style="background-color: transparent; ">
				&nbsp;</div>
			<div style="background-color: transparent; ">
				<ul>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						This website will help you find a homestay near the city you want to stay.</li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						You can experience what is it like to live in Japan and see a typical Japanese house.</li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						If you are lucky your host family may cook you a meal.</li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						In Japanese culture you suppose to take a shower before entering the bath!!!!!</li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						Take of your shoes before you enter the house.</li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						You need to say &quot;itadakimasu&quot;- いただきますbefore meal and &quot;Gochisousamadesuta&quot;ごちそうさまですたafter the meal. It mean &quot;let eat&quot; and &quot;Thank you for your meal&quot;.</li>
				</ul>
			</div>
			<div style="background-color: transparent; ">
				<div style="background-color: transparent; display: block; text-align: left; ">
					<img alt="" src="/images/Capsule_Hotels_inside.jpg" style="width: 266px; height: 200px; " /><img alt="" src="/images/Capsule_Hotels_out.jpg" style="width: 281px; height: 200px; " /></div>
			</div>
			<div style="background-color: transparent; ">
				<a href="http://9hours.jp/" rel="nofollow" style="color: rgb(0, 128, 187); " target="_blank">Capsule Hotels</a></div>
			<div style="background-color: transparent; ">
				<ul>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						It is cheap and convenience, cost around &yen;3000 all nigh or &yen;300~400 per hour.&nbsp;</li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						All capsule can only fit one person.</li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						Due to safety reason, some capsule hotels only for male.</li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						Luggage is store in locker.</li>
				</ul>
			</div>
			<div style="background-color: transparent; ">
				<div style="background-color: transparent; display: block; text-align: left; ">
					<img alt="" src="/images/Youth_hostel.jpeg" style="width: 267px; height: 200px; " /></div>
			</div>
			<div style="background-color: transparent; ">
				<a href="http://www.jyh.or.jp/" rel="nofollow" style="color: rgb(0, 128, 187); " target="_blank">Youth hostel</a></div>
			<div style="background-color: transparent; ">
				<ul>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						&nbsp;Around &yen;3000 if you are stay in central Tokyo area, but the price will go down a little bit as it go further away form the city.</li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						Dormitory style: usually share a public toilet, bathroom, kitchen and lounge.</li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						It is a good chance for you to meet other backpacker, and they often have very useful travel information for you.</li>
				</ul>
			</div>
			<div style="background-color: transparent; ">
				<div style="background-color: transparent; display: block; text-align: left; ">
					<img alt="" src="/images/Ryokan.jpeg" style="width: 275px; height: 183px; " /></div>
			</div>
			<div style="background-color: transparent; ">
				<span style="color: rgb(0, 0, 0); font-family: sans-serif; line-height: 19px; "><i><a href="https://sites.google.com/site/cyuhin/goog_1195214981" style="color: rgb(0, 128, 187); ">Ryokan</a></i><a href="https://sites.google.com/site/cyuhin/goog_1195214981" style="color: rgb(0, 128, 187); ">&nbsp;(</a><span lang="ja"><a href="http://www.jalan.net/" rel="nofollow" style="color: rgb(0, 128, 187); " target="_blank">旅館)</a></span></span></div>
			<div style="background-color: transparent; ">
				<ul>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						<span style="color: rgb(0, 0, 0); font-family: sans-serif; line-height: 19px; ">Traditional Japanese inn, with Tatami畳 and futon 布団- Japanese style mattes .</span></li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						<span style="color: rgb(0, 0, 0); font-family: sans-serif; line-height: 19px; ">It is usually located outside of city, so it is hard to find in Tokyo東京 but you can find it easily in Kyoto京都.</span></li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						<font color="#000000" face="sans-serif"><span style="line-height: 19px; ">Most of the Ryokan is more expensive, because they are located near sea or Onsen 温泉- hot spring. Which mean they have better view from the room and a nice hot spring bath.</span></font></li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						<font color="#000000" face="sans-serif"><span style="line-height: 19px; ">Most Ryokan include 2 traditional Japanese meal: breakfast and dinner.</span></font></li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						<font color="#000000" face="sans-serif"><span style="line-height: 19px; ">Dinner is scheduled at a given time, while you are eating staff will prepare your futon.</span></font></li>
				</ul>
			</div>
			<div style="background-color: transparent; ">
				<div style="background-color: transparent; display: block; text-align: left; ">
					<img alt="" src="/images/Minsyuku.jpeg" style="width: 280px; height: 200px; " /></div>
				<div style="background-color: transparent; display: block; text-align: left; ">
					<em style="font-style: normal; "><a href="http://www.jalan.net/a/minshuku/index.html" rel="nofollow" style="color: rgb(0, 128, 187); " target="_blank">Minsyuku (民宿)</a></em></div>
			</div>
			<div style="background-color: transparent; ">
				<ul>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						<font color="#000000" face="arial, sans-serif"><span style="font-size: small; ">A budget version of Ryokan, usually own by a family.</span></font></li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						<font color="#000000" face="arial, sans-serif"><span style="font-size: small; ">It is cheaper, but there are no room services.</span></font></li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						<font color="#000000" face="arial, sans-serif"><span style="font-size: small; ">Most of the the Minsyuku include 2 meal.</span></font></li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						<font color="#000000" face="arial, sans-serif"><span style="font-size: small; ">Share toilet, bathroom and lounge.</span></font></li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						<font color="#000000" face="arial, sans-serif"><span style="font-size: small; ">You suppose to prepare your futon before you sleep.</span></font></li>
				</ul>
			</div>
			<div style="background-color: transparent; ">
				<b>For long term visit in Japan: (More than a month)</b></div>
			<div style="background-color: transparent; ">
				<img alt="" src="/images/apartment-in-japan.jpeg" style="width: 258px; height: 200px; " /></div>
			<div style="background-color: transparent; ">
				Apartment</div>
			<div style="background-color: transparent; ">
				<ul>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						It usually cost around &yen;50,000 ~ 200,000 depend on the size and location.</li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						In Japan the size of a room is measure by the number of &nbsp;tatami in the room.</li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						In order to rent an apartment you need a Japanese to co-signing for you, you can either as a friend or pay a company to co-signing for you.</li>
				</ul>
			</div>
			<div style="background-color: transparent; ">
				<img alt="" src="/images/Guest_house.jpg" style="width: 267px; height: 200px; " /></div>
			<div style="background-color: transparent; ">
				<b><span style="font-weight: normal; ">Guest house</span></b></div>
			<div style="background-color: transparent; ">
				<ul>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						No co-signning is required.</li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						Single room or dormitory style, it cost around &yen;40000~50000 for a 6 tatami single room.</li>
					<li style="list-style-position: outside; list-style-type: disc; list-style-image: url(https://ssl.gstatic.com/sites/p/2fa342/system/app/themes/notebook/bullet.gif); ">
						Dormitory style cost around &yen;30000-40000 include all utility bill.</li>
				</ul>
				Here is a custom search engine for you to search apartment and guest house:</div>
		</div>
	</div>
</div>
<form action="http://www.google.com/cse" id="cse-search-box">
  <div>
    <input type="hidden" name="cx" value="partner-pub-6659414395214545:0472733301" />
    <input type="hidden" name="ie" value="UTF-8" />
    <input type="text" name="q" size="55" />
    <input type="submit" name="sa" value="Search" />
  </div>
</form>

<script type="text/javascript" src="http://www.google.com/coop/cse/brand?form=cse-search-box&amp;lang=en"></script>

</form>
