---
layout: post
title: Working in Japan
created: 1297645130
---
<div>
	Hello! Today&nbsp;I`m going to tell the story about how I came to Japan, and hopefully provide information for those that are thinking about coming to Japan to teach or work. I`ll try to include everything that I can, but I may forget some things so I`ll apologize for it now; sorry!</div>
<div>
	&nbsp;</div>
<div>
	Feel free to pass this on to people who are thinking of coming or are looking for advice.</div>
<div>
	&nbsp;</div>
<div>
	<em><strong>In College (2008-2009)</strong></em></div>
<div>
	So I had studied abroad in Japan and loved it. I had met a girl there and she had visited me numerous times and we had been dating for about a year. I didn`t have any other ideas about what I should do after I graduate except go to Japan. So through my school`s programs, I applied to the Japanese schools that we had good relationships with, with an expectation that I would be hired for one of the jobs. That was my first big mistake. I didn`t get any of the jobs and hadn`t been making any back-up plans. I was devastated and took it pretty hard. My girlfriend also took it hard but she kept on encouraging me to come. So I finished up college.</div>
<div>
	&nbsp;</div>
<div>
	<em><strong>Summer (2009)</strong></em></div>
<div>
	Everyday I went online and applied to jobs. Some days, it was only 1 or 2, and others I applied to 4 or 5. This went on for about a month and a half, and I hadn`t even come close. All of the jobs were looking for people who were already in Japan, and that presented a big problem. I realized that I was going to have to go over there to really get a job. So for the rest of the summer, I continued applying to jobs all over, and started working full-time at a local bakery to start saving money for my trip. My family had moved during the summer to a place right outside the city, but I had moved in with&nbsp;a college friend who had just bought a house in my neighborhood, Teresa. She knew that I was trying to save money to go to Japan and wanted to help, so she charged me really cheap rent and I`ll always be grateful for that kindness that she showed me.</div>
<div>
	&nbsp;</div>
<div>
	<em><strong>Fall (2009)</strong></em></div>
<div>
	I started saving money and started working for a local contractor&nbsp;on weekends and&nbsp;after I finished at the bakery some days. I realized then that I`d lost my passport (ridiculously stupid thing to do by the way) so I started the process of ordering a new one. As a procrastinator, I got everything ready but didn`t send it in, because in my mind, what was the rush? (again, I was young and stupid) My younger sister was getting married at the end of January, so I decided that I would leave after her wedding. I worked and worked and eventually saved enough money to get by for a month or two in Tokyo. My girlfriend said that I could stay with her, so I was just worried about buying food and other essentials.</div>
<div>
	&nbsp;</div>
<div>
	<strong><em>Winter (2009)(Minnesota)</em></strong></div>
<div>
	Everything was set, I had a round trip ticket (I`ll explain later) and I was excited to go. Then about 3 weeks before I was supposed to leave, my girlfriend of the time told me that things weren`t so good between us. That`s a whole `nother story but what it means in this story is that I no longer had a place to live. I contacted everyone that I could think of in Tokyo for help and just when I was resolving myself to stay at manga cafes and/or Ueno park for the nights, I heard back from a friend, Alex. He and another friend were working at the school that we`d studied abroad at. They had a small house that they were staying at that the school provided for them. They said for 30000yen a month ($300 roughly) I could stay with them for the 2 months that they would still be there. So that was the plan. I had about $2700 when all was said and done and took off to go to Japan.</div>
<div>
	&nbsp;</div>
<div>
	<em><strong>Winter (2010)(Tokyo)</strong></em></div>
<div>
	I arrived on Jan. 28th.&nbsp;Once again, everyday I was looking online for jobs and applying to as many as I could. Within 2 weeks of being here, I heard back from a couple companies and set up an interview with one. I had the interview and got the job, and would be starting&nbsp;on March 1st. I was going to be a Junior High English ALT (Assistant Language Teacher). I was only going to work for the month because I was&nbsp;just filling in for another teacher who had to leave early due to family problems.&nbsp;After the month was up though, I&nbsp;had a year&nbsp;long contract that put me in Narashino,&nbsp;Chiba&nbsp;as an Elementary School English ALT. After March, my friends who I was staying with left, and I moved into my girlfriends apartment with her until I could find something closer to my job.</div>
<div>
	&nbsp;</div>
<div>
	That`s basically my story, leaving out all of the juicy details, but here`s the part you`ll want to check out if your looking to come over here:</div>
<div>
	&nbsp;</div>
<div>
	<strong><em>Job sites</em></strong>-</div>
<div>
	craigslist.org -Tokyo Craigslist is a decent resource, as are the craiglists from all over Japan.</div>
<div>
	gaijinpot.com -Gaijinpot is handy because you can save your resume and cover letters and apply to tons of jobs easily. Many of the jobs want you to be in Japan already&nbsp;though.</div>
<div>
	daveseslcafe.com -This site has job listings from all over Asia. It`s pretty nice, but there aren`t usually&nbsp;many jobs in the&nbsp;Kanto Region (around Tokyo) If you don`t mind going somewhere far,&nbsp;then this site should be very helpful.</div>
<div>
	&nbsp;</div>
<div>
	<em><strong>Heart Corporation -</strong></em> the company that I work for now. They are good with their people,&nbsp;however the pay is a bit low. The low pay isn`t too bad at all&nbsp;and you get summer and winter&nbsp;holidays, so it`s pretty nice.</div>
<div>
	<em><strong>Interac</strong></em> - an ALT dispatch company that isn`t very hard to get a job with, but has pretty bad reviews from a lot of people.</div>
<div>
	<strong><em>Shane English School </em></strong>- one of my roommates is working here and really likes it. It`s a private school and you give lessons to people of all ages.</div>
<div>
	There are tons more too, but you gotta find them on your own!</div>
<div>
	&nbsp;</div>
<div>
	Other than that, there are tons of other sites that list multiple jobs,&nbsp;just search&nbsp;on google and you shouldn`t have too much trouble. The biggest thing to remember is that you&nbsp;have to apply apply apply.&nbsp;If you are just coming out of college, many of these places require 2 years of&nbsp;teaching and you won`t have that.&nbsp;Apply anyway. Don`t think too much about the required stuff (if they ask,&nbsp;I wouldn`t lie about it, but apply anyway!) and go nuts sending&nbsp;your resume out.</div>
<div>
	&nbsp;</div>
<div>
	<strong><em>Legal Stuff-</em></strong></div>
<div>
	One of the biggest problems about coming over here and staying is that&nbsp;visa issue.&nbsp;I came over&nbsp;on a tourist visa and after getting a job got a&nbsp;work visa.&nbsp;Coming on a tourist visa at first&nbsp;presents a few problems. One is that you must be able to prove that you are a tourist. That means that when you enter the country at customs, that you must be able to show that you are leaving the country sometime within the next 3 months.&nbsp;I`ve heard stories&nbsp;of&nbsp;people coming with a one ticket&nbsp;just to be turned around at customs because it was obvious what they were trying to do. So either prepare to come with the work visa already in hand, or come ready to change to a work visa. This means that you should bring your college transcripts (because to get the work visa, you have to prove that you have a bachelors degree) and some ID`s and stuff too. For the things that you really need, research on the America/Japan visa website. It costs a couple hundred bucks to get all of that done as well, so be prepared for that.</div>
<div>
	&nbsp;</div>
<div>
	Another thing is that when you get here, make sure that you get a Gaijin Card as soon as possible. It`s pretty easy, but I waited really long and it made things a little bit complicated. With a Gaijin card, you can get a phone and other important stuff, so just get it done with quickly.</div>
<div>
	&nbsp;</div>
<div>
	<strong><em>Money-</em></strong></div>
<div>
	One of the biggest shocks when I came over here, was that I had a lot less money than I planned. The exchange rate is pretty rough right now, and as such I thought that I was coming with 2700 bucks approximately. After the exchange, I had only about $2300 and that was a pretty big hit on me. Watch out for exchange rates.</div>
<div>
	Also, getting paid here works differently with every company, but I believe it is normal to get paid for the month of work, the following month. If that doesn`t make sense, I will clarify a bit: I started working in March. My company`s pay day is the 25th of every month. We get paid for the preceding month so even though I started work on March 1st, I didn`t get paid until February 25th. It`s a lot rougher than it sounds if you come without much money so be prepared to wait quite a long time for that first paycheck.</div>
<div>
	&nbsp;</div>
<div>
	<em><strong>Living-</strong></em></div>
<div>
	Finding a place to live can be a pain. If you have money, it isn`t too bad because around the Kanto region there are Guest houses all over. If you are living in smaller towns, you may have to find an apartment which can be trickier.</div>
<div>
	&nbsp;</div>
<div>
	<strong><em>Guesthouse</em></strong>-a guest house is basically a dorm. Typically, you get your own room and share a living room, kitchen and bathroom. Some are large houses that have been remodeled slightly to be more like a guesthouse and some are just like dorms. The place that I`m living now is like a dorm and I have roommates from all over the world (right now,&nbsp;1 Englishman, 3 Germans,&nbsp;1 other American,&nbsp;2 Japanese (one German was born and raised in Germany), and&nbsp;one Afghani (raised in Germany), so it`s pretty educational as well as fun. The place I`m living, is really cool and laid back, but not all guesthouses are like that. Some require that you always check in your guests and some have curfews. It`s all about finding a place that suits your needs. Besides a deposit, you usually don`t have any hidden costs or surprise expenses.</div>
<div>
	&nbsp;</div>
<div>
	<strong><em>Apartments-</em></strong>these are tough because you have to pay tons of money to get it all worked out. First you have to find a place. Then you must have a Guarantor, which has to be someone who has lots of money and can back you up. Then you have to pay the agents fee, for the agent finding the apartment (typically same as the first months rent). After that you have to pay the rent and also some sort of gift money for the owners of the building. It all seems like a huge pain and if you don`t speak any Japanese a much bigger one. If you are already here, or plan on staying for a long time, it probably wouldn`t be too bad a thing to find an apartment, but for just arriving, a guesthouse should be just fine.</div>
<div>
	&nbsp;</div>
<div>
	&nbsp;</div>
<div>
	That`s all that I have for now, and I`ll add some more things if I think of any. Hope this helps and good luck coming to Japan!</div>
