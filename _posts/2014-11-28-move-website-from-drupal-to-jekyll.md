---
layout: post
title:  "Move my website from Drupal to Jekyll"
date:   2014-11-28 16:43:00
categories: jekyll drupal 
---

Since I work with cloud infrastructure and DevOps on a daily basis, slow websites and servers are always haunting me. I would like my all my websites to load quickly, even for my personal blog. Although I love to work with Drupal's powerful framework, it costs too much time to keep up with the maintenance and plugin update. 

I decided to move my blog from Drupal to [Jekyll, a static website generator][jekyll]. The reason behind the migration is that I want to focus more on the blog's content instead of server maintenance. After the migration, the response time went from 3000ms down to 40ms, Now that is what I called improvement. A blog should only have one function - delivery of a message to the audience, nothing more.

One of the benefits of [Jekyll] is its importer; it allows you to import your old posts from different platform without any difficulties.

[jekyll]:      http://jekyllrb.com
