---
layout: post
title: The second year in Japan
created: 1343887344
---
<p>
	It is already been 2 years since I live and work in Japan, I can&#39;t believe that I stay here for that long. At the beginning, I was a student who study aboard for one semester. At the end of the semester I decided to stay here and experience more, working and enjoying my live in Japan.<!--break-->The first year was tough, trying to adapt to the new environment and language. Then the second year was easier and more enjoyable as I make many new friends and get the hang of the language. Beginning from today I will try to write this blog as often as I can, increase the length in the post and put useful information about Japan. I hope this website will help people in many ways, so you can learn from my mistake.</p>
<p>
	&nbsp;</p>
<p>
	After working in Japan for a little more then a year, I notice that management system in most Japanese companies are lack of flexibility and efficiency. Management hierarchy is deeply embedded into Japanese culture, lead to poor communication and lack of creativity. There is no doubt that Japanese companies are trying to introduce western management system into their company, but most worker are still believe in their traditional value and obedience to&nbsp;senior level worker. The management system work for most westerner because employees are willing to express their own opinion and the employer are willing to listen. In the case of Japanese companies, most employees will not confront their superior&nbsp;even they know there is a problem. Maybe I shouldn&#39;t blame on the Japanese culture, it is more the problem of the poor management. I am sure this happen in companies around the world, but Japanese companies tend to have this problem more often.</p>
<p>
	I work as a software engineer, mostly work with other engineer and designer. In this environment I personally think that a flat management system is quite important because we are creating a product together. I seem to run into many bottleneck because no one willing to listen my opinion because I am the youngest person in the development team, to be honest I am the one with the less experience but this doesn&#39;t make my opinion less important. On most projects, the resource -- ( images or design) I request was not fill by anyone because the manager told me that the designer are busy and have more important project working on. I know all the designers personally and they told me that they have nothing to work on. This is one of the example of poor management system in Japan, my suggestion to improve the management system in Japan is to remove senior hierarchy system and create a more competitive environment at the same time. Employees will feel more motivated to work on their project and create a better product.</p>
<p>
	One person I really admire is Steve Jobs, his management style is very strict and hard, but at the same time he is willing to listen to people. Some indie developer send him a email inquire about his app approval process, he personal reply the email and help this developer fix his problem. A CEO of a big company reply the email himself not happen very often.</p>
<p>
	I think this is the end of today&#39;s blog post, if you have any question about Japan or want to talk me about any thing. Feel free to reply below or send me an email.&nbsp;</p>
<p>
	&nbsp;</p>
<p>
	&nbsp;</p>
