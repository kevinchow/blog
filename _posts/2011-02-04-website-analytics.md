---
layout: post
title: Website analytics
created: 1296783530
---
<p>
	Since the website reopened on January 28, 2011, we had more than 120 visitors come from 10 countries all over the world. It is very successful for a website that only opened for less than 10 days. If you search &quot;Japan foreigner&#39;s guide&quot; in Google, our website is on the top 5 listing. I am glad that everything working out so smoothly. Once again special thanks to all of our editors for writing articles and updating the posts.</p>
<p>
	<img alt="" src="/images/Screen shot 2011-02-04 at 10_08_59 AM.png" style="width: 500px; height: 81px; " /></p>
<p>
	&nbsp;</p>
<div>
	In the last few days, I was so busy working on my part time job, creating a patch for an iPhone application. Also getting ready to publish an iPhone application that I am working on.&nbsp;So far, I haven&#39;t posted anything that is really interesting about Japan, but this weekend I am planning to write up a post about Japan&#39;s transportation and interesting food. If you came to visited this website and find nothing interested you, then you definitely should come back and check it out again. Our editors are working hard to look for interesting news, places and foods in Japan especially around Tokyo just for you.</div>
<div>
	&nbsp;</div>
<div>
	*** Updated on Feb 5, 2011&nbsp;&nbsp;***</div>
<div>
	&nbsp;</div>
<div>
	Now our website have visitor from more than 14 countries from the world!!!! Woo hooo.....</div>
