---
layout: post
title: Welcome our new editors !!!
created: 1296458940
---
<p>
	It already been two weeks since I stopped blogging, so it is time to start updating the blog again. After &quot;Japan Survival Guide&quot; reopen yesterday, I received a good news from my good friend who is willing to join out team as a editor. A special thank you to our editor&nbsp;Andrea Lewandowski and our art/sport editor Theo Hoglund&nbsp;for joining our team.</p>
<p>
	Building this website was not very difficult from the technical point of view, because the content management system in the server will handle most of the tasks. One of the most difficulties for this website is to find people to join the development team, especially our website is at the beginning stages. Most of the task don&#39;t take much time to finish, but it take a lot of motivation and spirit. It is very difficult to find a person that enjoy blogging at the same time with out their own blog, because in nowadays almost everyone can create their own blog in a matter of hours. In most of the case, 9 out of 10 people who like blogging will have their own blog.</p>
<p>
	It is my pleasure to have them in our website, I hope this website will keep on growing in the future. And bring more informations and interesting news to all the readers. Especially from our onsite artist/musician Theo, whom is skillful at painting and playing drums. He is also a member of a band based in Tokyo and a English teach.</p>
