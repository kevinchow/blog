---
layout: post
title: Japan survival guide grand opening
created: 1296393186
---
<p>
	About 10 days ago, I decided to rebuild my blog. I disconnected the domain from the old server and signed up for a new web hosting server. Due to my part time job I was unable to develop the new site immediately, so the website was down for the last two weeks until yesterday. I spent about 16 hours setting up the server and installing Drupal- a content management system into the server.<br />
	<br />
	I used to host my website using Google sites. I love Google&#39;s products because they are conveneint and reliable. It was a hard decision to change over to a new server, but Google had too many limitations on the website&#39;s functionality so I had to switch to a new one. Now I have more control of the website, so I can put my creativity into action. I can even set up the layout the way I like and use different kinds of technology in this website. I hope to bring the best experience I can to my users when they browse this website. As a registered user you can comment on the posts and talk to other users. In the future, I hope to add more functionality to this website when there are more readers. As for now, I have added polls, comments, photos upload, advance text editor, post filter and much more.<br />
	<br />
	It is my goal to upload approximately 2 blog posts each week, but it really depends on my schedule. I am also looking for people who are interested in Japan&#39;s culture, food, and places to blog on this website. If you want to share your experiences with others, please feel free to create an account and leave a post. I would be happy to meet new people and help others. If you have any questions about living or traveling in Japan, please leave me a comment and I will do my best to help you.<br />
	<br />
	In the following weeks, I will do my best to upload at least 1 post a week no matter how busy I am. I have a lot of topics to write about, but no time to write it out, now that the basic functionality of this website is almost completed I can spend more time writing posts here.</p>
