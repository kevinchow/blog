---
layout: post
title: Drupal 7 migration to Amazon AWS EC2 micro instance
created: 1359786039
---
<p class="p1">Earlier this week my shared hosting for this blog was&nbsp;almost at the end of its contract, so I decided to upgrade my server to a cloud VPS&nbsp;hosting. This will improve the loading speed&nbsp;for this blog and allow me to experiment&nbsp;with my other websites. During the migration process there were&nbsp;quite a few problems with the blog&#39;s CMS system - Drupal 7, because earlier there was a lot of spam in this blog so I directly delete the spam users, blogs and comments in database. Ooooo boy, after that I realize it is not a good idea to directly edit the record in the database because the CMS system cannot update the record. The proper way to delete spam user and all of their contents is to use the administrator&#39;s panel or type in this URL [www.yourWebsite.com]/admin/people&quot;.</p>

<p class="p1">Well, if you are here, that mean you have some trouble. Let get start and fix it, after you deleted the spam&nbsp;comment&nbsp;(truncate comment)&nbsp;&nbsp;in your database the record count in your database is not correct, you may noticed&nbsp;the&nbsp;comment counter&nbsp;in your post is not updated and when you add a new comment the following error appear.</p>

<blockquote>
<p class="p2"><i>Undefined property: stdClass::$comment_count in comment_node_page_additions()</i></p>
</blockquote>

<p class="p3"><span class="s1">This error mean that your&nbsp;</span>node_comment_statistics table is empty, in order to fix this problem we need to run a query in mysql database.</p>

<blockquote>
<p class="p3">INSERT INTO `node_comment_statistics` (`nid`, `cid`, `last_comment_timestamp`, `last_comment_name`, `last_comment_uid`, `comment_count`)<br />
SELECT nid, 0, UNIX_TIMESTAMP(), NULL, 1, 0 FROM node</p>
</blockquote>

<p class="p3">What this query doing is add a record to all the node in your database, this should fix the problem for you.</p>
