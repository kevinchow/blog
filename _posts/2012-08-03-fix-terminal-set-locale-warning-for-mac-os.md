---
layout: post
title: Fix terminal set locale warning for Mac OS
created: 1343960111
---
<p>When I try to ssh into my remote server using terminal it give me the following warning.</p>

<blockquote>
<p><span style="background-color:#ffff00;">-bash: warning: setlocale: LC_CTYPE: cannot change locale (UTF-8)</span></p>
</blockquote>

<p>It doesn&#39;t happen to every server, but just one particular server. In order to fix this problem, you need to add the following line <font color="#333333" face="Tahoma, Verdana, Arial, sans-serif"><span style="font-size: 12px; line-height: 21.600000381469727px;">into .bash_profile in your computer not your server.</span></font></p>

<p>&nbsp;</p>

<blockquote>
<div><span style="background-color:#00ff00;">export LC_CTYPE=en_US.UTF-8</span></div>

<div><span style="background-color:#00ff00;">export LC_ALL=en_US.UTF-8</span></div>
</blockquote>

<div>&nbsp;</div>

<div>After that save and restart your terminal session, then try to ssh into your server again. You should not see the warning again.</div>
