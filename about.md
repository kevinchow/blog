---
layout: page
title: About
permalink: /about/
---

Hello there, this is Kevin. I graduated from Saint John's University with a major in Computer Science. It is my dream to become a computer scientist when I was little, by the time I enter college I already make up my mind to learn more about computer technology.

 
I interested in High-performance computing, web development and Artificial intelligence. After I graduated, I decided to visit Japan and learn Japanese. Four months later, I fall in love with Tokyo and decided to stay in Japan looking for a job even though my Japanese isn't fluently. Another four months later, I am working at Bijin Tokei as a Software Engineer makind iOS and Android Apps.

 
When I am free, I like to take photo with my camera and go to rock climbing. I love outdoor activity, especially volleyball, camping and hiking. Also a foodies who love food, so I always cook and experiment new ideas with different kind of ingredients. The purpose of this website is to express my ideas, testing new technology and help others to survive in Japan.

 
What I see myself in 5 to 10 years later.
I want to start a web/software development company and
travel 3 months in a year.
